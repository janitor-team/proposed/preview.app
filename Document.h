/*
** Document.h
**
** Document header : This file is part of Preview 
** 
** Copyright (c) 2003,2004 Fabien VALLON 
** 2003,2004 Alcove ( http://www.alcove.com ) 
** Additional copyrights here
** 
** Authors : Fabien VALLON <fabien@sonappart.net>
** Date:	10 Oct 2003
** 
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License as
** published by the Free Software Foundation; either version 2 of
** the License, or (at your option) any later version.
** 
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
** 
** See the GNU General Public License for more details.
** 
** You should have received a copy of the GNU General Public
** License along with this program; if not, write to:
** 
** Free Software Foundation, Inc.
** 59 Temple Place - Suite 330
** Boston, MA  02111-1307, USA
*/

#ifndef _IMAGESAPPS_PREVIEW_DOCUMENT_H
#define _IMAGESAPPS_PREVIEW_DOCUMENT_H

#include <Foundation/NSGeometry.h>
#include <AppKit/NSDocument.h>

@class NSImage;
@class NSImageView;
@class NSNotification;
@class NSMatrix;
@class NSScrollView;
@class NSWindow;


/*!
  @class Document
  @abstract Preview NSDocument subclass
  @discussion Preview NSDocument subclass. 
  It is also the Preview.gorm controller
  and the window delegate
*/
@interface Document: NSDocument
{
  NSScrollView *scrollView;
  NSImageView *imageView; 
  NSWindow *window;
  NSPopUpButton *popUp;
  NSMatrix *matrix;
@private 
  NSImage *_image;
  double _scaleFactor;
  NSSize _originalSize;
  unsigned _tag;
  BOOL _isAlpha;
  id checkeredView;
  id openHandCursor;
  id closedHandCursor;
}

//Action methods
-(void) zoomImage: (id) sender;  //from menuItems
-(void) resize: (id) sender;     //from menuItems/popUp/matrix

//window delegate
- (void)windowDidResize:(NSNotification *)aNotification; 
- (void)windowDidMiniaturize:(NSNotification *)aNotification; 

@end

#endif /* _IMAGESAPPS_PREVIEW_DOCUMENT_H */
