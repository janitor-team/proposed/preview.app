 /*
        ImageView.h

	This file is part of Preview 

	Copyright (C) 2003;2004 Fabien VALLON 
	2003,2004 Alcove ( http://www.alcove.com ) 
	Additional copyrights here

	Authors : Fabien VALLON <fabien@sonappart.net>
	Date:	10 Jan 2005

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License as
	published by the Free Software Foundation; either version 2 of
	the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

	See the GNU General Public License for more details.

	You should have received a copy of the GNU General Public
	License along with this program; if not, write to:

		Free Software Foundation, Inc.
		59 Temple Place - Suite 330
		Boston, MA  02111-1307, USA
*/

#include "ImageView.h"
#include <Foundation/NSNotification.h>
#include <Foundation/NSDictionary.h>
#include <Foundation/NSValue.h>

#include <AppKit/NSCursor.h>
#include <AppKit/NSEvent.h>

@implementation ImageView 

-(BOOL) needsHandCursor
{
  NSSize visibleSize = [self visibleRect].size;
  NSSize boundsSize  = [self bounds].size;
  
  if ( ( ( visibleSize.width + 1 )  < boundsSize.width ) 
       || ( ( visibleSize.height + 1 ) < boundsSize.height ) )
    return YES;
  else
    return NO;
} 

- (void) mouseDown: (NSEvent*)event
{
  if ( [self needsHandCursor] ) 
    {
     [[NSCursor closedHandCursor] push];
    }
}

- (void) mouseUp: (NSEvent*)event
{
  if ( [self needsHandCursor] ) 
    {
     [[NSCursor openHandCursor] push];
    }
}

- (void) mouseDragged: (NSEvent*)event
{
   NSSize scrollAmount = NSMakeSize(-[event deltaX], -[event deltaY]);
   NSDictionary *dict = [NSDictionary dictionaryWithObject: 
				       [NSValue valueWithSize: scrollAmount]
				     forKey: @"UserInfoKeyScrollAmount"];

   [[NSNotificationCenter defaultCenter] postNotificationName: @"TEST"
					 object: self
					 userInfo: dict];
}



-(void) mouseEntered:(NSEvent *) event
{
  if ( [self needsHandCursor] ) 
    [[NSCursor openHandCursor] push];
//   else
//     [[NSCursor arrowCursor] push];
}

-(void) mouseExited:(NSEvent *) event
{
   if ( [self needsHandCursor] ) 
     {
       [[NSCursor arrowCursor] push];
     }
}


@end
